#! python3

import os
import os.path
import xml.dom.minidom
import shutil
import sys
from traceback import print_exc

from xml.etree import ElementTree as ET


def xml_to_etree(xml_filename):
    etree = None
    lines = open(xml_filename,encoding="latin-1").read().splitlines()
    text = None
    if xml_filename.endswith(".mht"):
        # Exported as 'Single page HTML file'
        lines=lines[10:-3]
        lines = [line for line in lines if "<meta" not in line]   
        lines = [line for line in lines if "<link" not in line]   
        lines = [line for line in lines if "<style" not in line]   
        text = "\n".join(lines)
        text = text.replace("=3D","=")
        lines = text.splitlines()
        print(lines[0],lines[-1])
    elif xml_filename.endswith(".xml"):
        # Exported as 'Microsoft Excel 2004 XML'
        text = "\n".join(lines)
        text = text.replace("{urn:schemas-microsoft-com:office:excel}","")
        lines = text.splitlines()
    try:
        etree = ET.fromstringlist(lines) # xml.etree
    except AttributeError:
        etree = ET.parse(text) # lxml.etree
    return etree

def etree_node_info(etree,max_depth=1):
    node_info = {}    
    def extract_ni(etree,depth):
        if depth==max_depth:
            return
        for element in list(etree):
            element.tag = element.tag.split('}', 1)[-1]
            ni_key = (depth,element.tag)
            ni_value = None
            node_info[ni_key]=ni_value
            extract_ni(element,depth+1)
    extract_ni(etree,0)
    [ print(ni) for ni in sorted(node_info.keys()) ]

field_names = None
def simplify_excel2004(etree):
    def _simplify(etree):
        global field_names
        for element in list(etree):
            element.tag = element.tag.split('}', 1)[-1]
            if element.tag == 'Row':
                if field_names is None:
                    field_names = [ f[0].text.replace(" ","_") for f in list(element) ]
                    etree.remove(element)
                else:
                    _simplify(element)
                    for i in range(0,len(list(element))):
                        subelement = list(element)[i]
                        if len(subelement)>0:
                            subelement.text=subelement[0].text
                            subelement.remove(subelement[0])
                    field_values = [ f.text for f in list(element) ]
                    for i in range(0,len(element)):
                        element[i].tag=field_names[i]
                        # element[i].text=field_values[i]
                element.attrib = {}
            elif element.tag not in ('Worksheet','Table','Row','Cell','Data'):
                etree.remove(element)
            elif element.tag=='XCell': #Doesn't work yet
                data = element[0]
                etree.remove(element)
                data.attrib = {}
                etree.insert(0,data)
            else:
                element.attrib = {}
                _simplify(element)
    _simplify(etree)
    return etree


def export_csv(etree,csv_fname,topic,recog_string):
    export = False
    idclient = None
    def _export(etree):
        global export
        global idclient
        if etree.tag == 'Row':
            export = False
            idclient = None
        for element in list(etree):
            if element.tag == 'IDClient':
                idclient = element.text
            elif recog_string.lower() in str(element.text).lower():
                export = True
            try:
                _export(element)
            except TypeError:
                pass
        if etree.tag == 'Row' and export is True:
            print(
                "%s,%s" % ( idclient, topic),
                file=open(csv_fname,"a")
            )
            # _anonymize(element)
    print("Exporting data on %s to %s" % (
        topic, csv_fname
    ))
    _export(etree)

def anonymize(etree):
    ANON_PII_FREE_FIELDS = ( 
        'Worksheet','Table','Row',
        'Suburb','State','Country','Postcode',
        'Client_Type','Industry',
    )
    def _anonymize(etree):
        idclient = None
        for element in list(etree):
            if element.tag == 'IDClient':
                idclient = element.text
            elif element.tag in ANON_PII_FREE_FIELDS:
                # print("PII free",element.tag,element.text)
                pass
            elif element.text:
                substring = element.tag + idclient + " "
                substring_reps = int(1+len(element.text)/len(substring))
                element.text = substring * substring_reps
            _anonymize(element)
            # _anonymize(element)
    print("Anonymizing data before full export")
    _anonymize(etree)
    return etree

def extract(xml_filename,anon=False):
    subdir = "full"
    if anon is True:
        subdir = "anon"
    if os.path.exists(subdir):
        shutil.rmtree(subdir)
    os.mkdir(subdir)
    xml2_filename = os.path.join(subdir,"export.xml")
    print("Converting",xml_filename,"to",xml2_filename)
    etree = None
    etree = xml_to_etree(xml_filename)
    # etree_node_info(etree,6)
    etree=simplify_excel2004(etree)
    interest_csv_fname = os.path.join(subdir,"interests.csv")
    export_csv(etree,interest_csv_fname,"behaviour","behav")
    export_csv(etree,interest_csv_fname,"behaviour","pbs")
    export_csv(etree,interest_csv_fname,"autism","autis")
    export_csv(etree,interest_csv_fname,"autism","asd")
    export_csv(etree,interest_csv_fname,"abi","abi")
    export_csv(etree,interest_csv_fname,"guardianship","guard")
    export_csv(etree,interest_csv_fname,"education","edu")
    export_csv(etree,interest_csv_fname,"communication","comm")
    export_csv(etree,interest_csv_fname,"advocacy","advoc")
    export_csv(etree,interest_csv_fname,"fasd","fasd")
    export_csv(etree,interest_csv_fname,"rett","rett")
    if anon is True:
        etree=anonymize(etree)
    # etree_node_info(etree,6)
    minidom_xml = xml.dom.minidom.parseString(ET.tostring(etree, encoding="utf-8")) 
    print(
        minidom_xml.toprettyxml().replace("\n\t\t\t\t    ",""),
        file=open(xml2_filename,"w",encoding="utf-8")
    )
                    
                    
#raise RuntimeError("X")
if __name__ == "__main__":
    try:
        infile = None
        if len(sys.argv)>=1:
            infile = "DDWA - CRM extract data.xml"
        else:
            infile = sys.argv[0]         
        extract(infile,True)
        extract(infile,False)
    except:
        print_exc(6)
