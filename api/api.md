# API Documentation

Within the Wordpress site, a new endpoint has been added to consolidate the datasets from WooCommerce, CRM (GTPHub) and Registration Form (NinjaForm).

## Endpoints

The following endpoints have been registered.  The URL endpoint (base) is: 

```` 
https://ddwa.org.au/wp-json
````

### Members - Master

````
URL:  /members/0100
PHP File:  members.php
PHP Function:  membersAPI
````

### Members - From CRM

````
URL:  /members/0100/crm
PHP File:  crm.php
PHP Function: memberCRMAPI
````

### Members - From Woo (eCommerce)

````
URL:  /members/0100/orders
PHP File:  woomembers.php
PHP Function: memberOrdersAPI
````

### Orders - From Woo (eCommerce)

````
URL:  /orders/0100/
PHP File:  orders.php
PHP Function: ordersAPI
````



### Members - From Registration Form (Ninja)

````
URL:  /members/0100/
PHP File:  registrationmembers.php
PHP Function: memberRegistrationAPI
````
