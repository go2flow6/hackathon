<?php

    function membersAPI( $request ) 
    {
        $query = $_SERVER['QUERY_STRING'];
        
        if(is_numeric($query)){
            return get_member_by_id($query);
        }else{
            if($query == "csv")
            {
                return members_to_csv();  
            }else{
                return get_members();
            }
        }    
    }

    function members_to_csv()
    {        
        $memberInfo = get_members();

        $csvFileName = generateRandomString() . '.csv';

        //Open file pointer.
        $fp = fopen($csvFileName, 'w');
        
        fputcsv($fp, ["Last Name", "First Name", "Middle Names", "Salutation", "Position", "Organisation", "Address1", "Address2", "Address3", "Suburb", "State", "Postcode", "Country", "Option1", "Phone1", "Phone2", "Phone3", "Phone4", "Phone5", "Email", "Notes", "Web Address", "Client Type", "Industry", "Is contact", "Keyword", "External Ref ID"]);
        fputcsv($fp, '\n');
        //Loop through the associative array.
        foreach($memberInfo->data as $row)
        {
            fputcsv($fp, $row->data);
            fputcsv($fp, '\n');
        }
        
        //Finally, close the file pointer.
        fclose($fp);

        header('Content-type: application/csv');
        header("Content-Disposition: inline; filename=".$csvFileName);

        readfile($csvFileName);
    }

    function generateRandomString($length = 10) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    function get_members()
    {
        $wooOrdersUrl = 'https://ddwa.org.au/wp-json/wc/v2/orders';
        // Basic Auth
        $auth = base64_encode("ck_95935a2660f164bbdff651c9c7fb7393cff3e6b3:cs_2aa2e54495133e192f4c412b293ed20dc9803973");
        $context = stream_context_create(['http' => ['header' => "Authorization: Basic $auth"]]);
        $wooOrdersContent = file_get_contents($wooOrdersUrl, false , $context);

        $wooOrdersRawJSON = json_decode($wooOrdersContent);
        $orderMembers = [];
        foreach($wooOrdersRawJSON as $order)
        {
            array_push($orderMembers, $order->customer_id);
        }

        $members = array_unique($orderMembers);

        $uniqueMembers = [];
        foreach($members as $member)
        {
            array_push($uniqueMembers, get_member_by_id($member));
        }

        return rest_ensure_response($uniqueMembers);
    }

    function get_member_by_id($memberId){
        $wooCustomersURL = 'https://ddwa.org.au/wp-json/wc/v2/customers/' . $memberId;
        // Basic Auth
        $auth = base64_encode("ck_95935a2660f164bbdff651c9c7fb7393cff3e6b3:cs_2aa2e54495133e192f4c412b293ed20dc9803973");
        $context = stream_context_create(['http' => ['header' => "Authorization: Basic $auth"]]);
        $wooCustomersContent = file_get_contents($wooCustomersURL, false , $context);
        $wooCustomersRawJSON = json_decode($wooCustomersContent);

        if($wooCustomersRawJSON->data->status == 404)        
        {
            return;
        }

        $member = [
            'lastName' => $wooCustomersRawJSON->last_name,
            'firstName' => $wooCustomersRawJSON->first_name,
            'middleNames' => '',
            'salutation' => '',
            'position' => '',
            'organisation' => '',
            'address1' => $wooCustomersRawJSON->billing->address_1, 
            'address2' => $wooCustomersRawJSON->billing->address_2,
            'address3' => '',
            'suburb' => $wooCustomersRawJSON->billing->city,
            'state' => $wooCustomersRawJSON->billing->state,
            'postcode' => $wooCustomersRawJSON->billing->postcode,
            'country' => $wooCustomersRawJSON->billing->country,
            'option1' => '',
            'phone1' => $wooCustomersRawJSON->billing->phone,
            'phone2' => '',
            'phone3' => '',
            'phone4' => '',
            'phone5' => '',
            'email' => $wooCustomersRawJSON->email == null ? $wooCustomersRawJSON->billing->email : $wooCustomersRawJSON->email,
            'notes' => '',
            'webAddress' => '',
            'clientType' => '',
            'industry' => '',
            'isContact' => 'yes',
            'keyword' => 'customer|client',
            'externalRefId' => (string)$wooCustomersRawJSON->id
        ];
        
        return rest_ensure_response($member);
    }

    function memberRegistrationAPI( $request )
    {

    }

    function memberOrdersAPI( $request )
    {
        
    }

?>