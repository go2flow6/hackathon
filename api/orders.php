<?php

function ordersAPI( $request ) 
{
    $wooOrdersURL = 'https://ddwa.org.au/wp-json/wc/v2/orders';
    // Basic Auth
    $auth = base64_encode("ck_95935a2660f164bbdff651c9c7fb7393cff3e6b3:cs_2aa2e54495133e192f4c412b293ed20dc9803973");
    $context = stream_context_create(['http' => ['header' => "Authorization: Basic $auth"]]);
    $wooOrdersContent = file_get_contents($wooOrdersURL);
    $wooOrdersRawJSON = json_decode($wooOrdersContent,true, $context);
    
    $orders = [];
    foreach($wooOrdersRawJSON as $order) {
        $orders[] = [
            'orderID' => $order->id;
            'dateCreated' => $order->date_created;
            'currency' => $order->currency;
            'total' => $order->total;
            'customerID' => $order->customer_id;
            'paymentMethod' => $order->payment_method_title;
            'datePaid' => $order->date_paid;
            'dateCompleted' => $order->date_completed;
        ];
    }

    return rest_ensure_response($orders);
}
?>
