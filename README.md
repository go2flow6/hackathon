# Hackathon

Welcome to DDWA Perth 2018

# A repo for anything worked on during #go2flow6

## API Documentation

Within the **API** folder are the documentation sets for the APIs/integration points.

## XLSX ETL

Within the **xlsx_etl** folder is a python script for converting the Excel extract from GTPHub into structured XML data.
Requires the extract be saved from Excel into .xml ('Excel 2004 XML') format.
The intent of the transformation is to remove noise from the XML exported from Excel.
Contributors: Tim Littlefair, others welcome to join

